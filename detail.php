<?php include 'header.php';
include 'breadcrumbs.php' ?>
    <div class="product-detail">
        <div class="container">
            <div class="row">
                <div class="column6">

                    <div class="photos">
                        <ul id="album" class="thumbnail photo-list">
                            <li><a href="#" data-image="assets/images/detail-1.jpg"
                                   data-zoom-image="assets/images/detail-1.jpg">
                                    <img src="assets/images/detail-1.jpg" alt="">
                                </a></li>
                            <li><a href="#" data-image="assets/images/detail-2.jpg"
                                   data-zoom-image="assets/images/detail-2.jpg">
                                    <img src="assets/images/detail-2.jpg" alt="">
                                </a></li>
                            <li><a href="#" data-image="assets/images/detail-3.jpg"
                                   data-zoom-image="assets/images/detail-3.jpg">
                                    <img src="assets/images/detail-3.jpg" alt="">
                                </a></li>
                            <li><a href="#" data-image="assets/images/detail-4.jpg"
                                   data-zoom-image="assets/images/detail-4.jpg">
                                    <img src="assets/images/detail-4.jpg" alt="">
                                </a></li>

                        </ul>
                        <div class="mainimage">
                            <figure>
                                <img src="assets/images/detail-1.jpg" id="zoom"
                                     data-zoom-image="assets/images/detail-1.jpg" height="auto" width="100%" alt=""
                                     style="position: relative">
                            </figure>
                        </div>


                    </div>
                </div>
                <div class="column6">
                    <div class="productDetails">
                        <div class="titleName">Slim Fit Coat
                            <small>Ruffle Sleeve Crop Sweatshirt</small>
                        </div>
                        <div class="socialshare">
                            <label for="">Share :</label>
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            </ul>
                        </div>
                        <div class="cost">
                            <div class="newcost">Rs:<span>6,166.25 NRP</span>
                            </div>
                            <div class="oldcost">Rs:<span>6,166.25 NRP</span>
                            </div>

                        </div>
                        <div class="quantity">
                            <div class="label">Quantity:</div>
                            <div class="field">
                                <div class="number">
                                    <button type="button" id="sub1" class="sub_qty left"><i
                                                class="fa fa-minus minus"></i></button>
                                    <input type="text" id="1" value="1" class="qty center" disabled="">
                                    <button type="button" id="add1" class="add_qty right"><i class="fa fa-plus add"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="shipping">
                            <div class="label">Shipping Cost:</div>
                            <div class="field">
                                <p>Free Shipping inside Kathmandu Valley</p>
                            </div>
                        </div>
                        <div class="btn-cart clearfix">
                            <button type="submit" class="addcart">Add to Cart</button>
                            <button type="submit" class="buy">Buy Now</button>

                        </div>
                    </div>
                </div>
            </div>

            <div class="row tabs-description ">
                <div class="column12">
                    <input id="tab1" type="radio" name="tabs" checked>
                    <label for="tab1">Codepen</label>

                    <input id="tab2" type="radio" name="tabs">
                    <label for="tab2">Dribbble</label>

                    <input id="tab3" type="radio" name="tabs">
                    <label for="tab3">Stack Overflow</label>

                    <input id="tab4" type="radio" name="tabs">
                    <label for="tab4">Bitbucket</label>

                    <section id="content1">
                        <p>
                            Jerky jowl pork chop tongue, kielbasa shank venison. Capicola shank pig ribeye leberkas filet mignon brisket beef kevin tenderloin porchetta. Capicola fatback venison shank kielbasa, drumstick ribeye landjaeger beef kevin tail meatball pastrami prosciutto pancetta. Tail kevin spare ribs ground round ham ham hock brisket shoulder. Corned beef tri-tip leberkas flank sausage ham hock filet mignon beef ribs pancetta turkey.
                        </p>
                        <p>
                            Bacon ipsum dolor sit amet landjaeger sausage brisket, jerky drumstick fatback boudin.
                        </p>
                    </section>

                    <section id="content2">
                        <p>
                            Bacon ipsum dolor sit amet landjaeger sausage brisket, jerky drumstick fatback boudin.
                        </p>
                        <p>
                            Jerky jowl pork chop tongue, kielbasa shank venison. Capicola shank pig ribeye leberkas filet mignon brisket beef kevin tenderloin porchetta. Capicola fatback venison shank kielbasa, drumstick ribeye landjaeger beef kevin tail meatball pastrami prosciutto pancetta. Tail kevin spare ribs ground round ham ham hock brisket shoulder. Corned beef tri-tip leberkas flank sausage ham hock filet mignon beef ribs pancetta turkey.
                        </p>
                    </section>

                    <section id="content3">
                        <p>
                            Jerky jowl pork chop tongue, kielbasa shank venison. Capicola shank pig ribeye leberkas filet mignon brisket beef kevin tenderloin porchetta. Capicola fatback venison shank kielbasa, drumstick ribeye landjaeger beef kevin tail meatball pastrami prosciutto pancetta. Tail kevin spare ribs ground round ham ham hock brisket shoulder. Corned beef tri-tip leberkas flank sausage ham hock filet mignon beef ribs pancetta turkey.
                        </p>
                        <p>
                            Bacon ipsum dolor sit amet landjaeger sausage brisket, jerky drumstick fatback boudin.
                        </p>
                    </section>

                    <section id="content4">
                        <p>
                            Bacon ipsum dolor sit amet landjaeger sausage brisket, jerky drumstick fatback boudin.
                        </p>
                        <p>
                            Jerky jowl pork chop tongue, kielbasa shank venison. Capicola shank pig ribeye leberkas filet mignon brisket beef kevin tenderloin porchetta. Capicola fatback venison shank kielbasa, drumstick ribeye landjaeger beef kevin tail meatball pastrami prosciutto pancetta. Tail kevin spare ribs ground round ham ham hock brisket shoulder. Corned beef tri-tip leberkas flank sausage ham hock filet mignon beef ribs pancetta turkey.
                        </p>
                    </section>
                </div>
            </div>
        </div>
    </div>
    <section class="hitStore_main_new_arrivals">
        <div class="container">
            <div class="row">
                <div class="column12">
                    <div class="section-title" data-title="Similar Products">
                        <h2>Similar Products</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="column12">
                    <div class="box">
                        <div class="product-list-inner">
                            <div class="product-listing">
                                <!-- Image -->
                                <figure>
                                    <span class="label sale">sale</span>
                                    <a href="#" class="img"><img src="assets/images/product-2.png" alt="Product Image"></a>
                                    <div class="product_hover_info">
                                        <ul class="product_action">
                                            <li><a title="View" href="#"><i class="fa fa-eye"></i></a>
                                            </li>
                                            <li><a title="Add TO Cart" href="#"><i class="fa fa-shopping-cart"></i></a>
                                            </li>
                                            <li><a title="Wishlist" href="#"><i class="fa fa-heart"></i></a></li>
                                        </ul>
                                    </div>
                                </figure>

                                <!-- Content -->
                                <div class="product-content">
                                    <!-- Category & Title -->
                                    <div class="product-category-title">
                                        <a href="#" class="product-cat">Camera</a>
                                        <h5 class="product-title"><a href="#">Mony Handycam Z 105</a></h5>
                                    </div>
                                    <!-- Price & Ratting -->
                                    <div class="price-ratting">
                                        <h5 class="price">$110.00</h5>
                                        <div class="ratting">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-half-o"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box">
                        <div class="product-list-inner">
                            <div class="product-listing">
                                <!-- Image -->
                                <figure>
                                    <span class="label sale">sale</span>
                                    <a href="#" class="img"><img src="assets/images/product-2.png" alt="Product Image"></a>
                                    <div class="product_hover_info">
                                        <ul class="product_action">
                                            <li><a title="View" href="#"><i class="fa fa-eye"></i></a>
                                            </li>
                                            <li><a title="Add TO Cart" href="#"><i class="fa fa-shopping-cart"></i></a>
                                            </li>
                                            <li><a title="Wishlist" href="#"><i class="fa fa-heart"></i></a></li>
                                        </ul>
                                    </div>
                                </figure>

                                <!-- Content -->
                                <div class="product-content">
                                    <!-- Category & Title -->
                                    <div class="product-category-title">
                                        <a href="#" class="product-cat">Camera</a>
                                        <h5 class="product-title"><a href="#">Mony Handycam Z 105</a></h5>
                                    </div>
                                    <!-- Price & Ratting -->
                                    <div class="price-ratting">
                                        <h5 class="price">$110.00</h5>
                                        <div class="ratting">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-half-o"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box">
                        <div class="product-list-inner">
                            <div class="product-listing">
                                <!-- Image -->
                                <figure>
                                    <span class="label sale">sale</span>
                                    <a href="#" class="img"><img src="assets/images/product-2.png" alt="Product Image"></a>
                                    <div class="product_hover_info">
                                        <ul class="product_action">
                                            <li><a title="View" href="#"><i class="fa fa-eye"></i></a>
                                            </li>
                                            <li><a title="Add TO Cart" href="#"><i class="fa fa-shopping-cart"></i></a>
                                            </li>
                                            <li><a title="Wishlist" href="#"><i class="fa fa-heart"></i></a></li>
                                        </ul>
                                    </div>
                                </figure>

                                <!-- Content -->
                                <div class="product-content">
                                    <!-- Category & Title -->
                                    <div class="product-category-title">
                                        <a href="#" class="product-cat">Camera</a>
                                        <h5 class="product-title"><a href="#">Mony Handycam Z 105</a></h5>
                                    </div>
                                    <!-- Price & Ratting -->
                                    <div class="price-ratting">
                                        <h5 class="price">$110.00</h5>
                                        <div class="ratting">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-half-o"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box">
                        <div class="product-list-inner">
                            <div class="product-listing">
                                <!-- Image -->
                                <figure>
                                    <span class="label sale">sale</span>
                                    <a href="#" class="img"><img src="assets/images/product-2.png" alt="Product Image"></a>
                                    <div class="product_hover_info">
                                        <ul class="product_action">
                                            <li><a title="View" href="#"><i class="fa fa-eye"></i></a>
                                            </li>
                                            <li><a title="Add TO Cart" href="#"><i class="fa fa-shopping-cart"></i></a>
                                            </li>
                                            <li><a title="Wishlist" href="#"><i class="fa fa-heart"></i></a></li>
                                        </ul>
                                    </div>
                                </figure>

                                <!-- Content -->
                                <div class="product-content">
                                    <!-- Category & Title -->
                                    <div class="product-category-title">
                                        <a href="#" class="product-cat">Camera</a>
                                        <h5 class="product-title"><a href="#">Mony Handycam Z 105</a></h5>
                                    </div>
                                    <!-- Price & Ratting -->
                                    <div class="price-ratting">
                                        <h5 class="price">$110.00</h5>
                                        <div class="ratting">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-half-o"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php include 'footer.php' ?>
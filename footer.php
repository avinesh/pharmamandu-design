<footer class="hitStore_footer">
    <div class="hitStore_main_footer">
        <div class="container">
            <div class="row">
            <div class="column3  footer-widget">
                <aside class="widget widget_text">
                    <h3 class="widget-title links-title">CONTACT INFO</h3>
                    <div class="textwidget">
                        <p class="contact-info">
                            <span>Address</span>
                            You address will be here <br>
                            Lorem Ipsum text </p>

                        <p class="contact-info">
                            <span>Phone</span>
                            <a href="tel:01234567890">01234 567 890</a>
                            <a href="tel:01234567891">01234 567 891</a>
                        </p>

                        <p class="contact-info">
                            <span>Web</span>
                            <a href="mailto:info@example.com">info@example.com</a>
                            <a href="#">www.example.com</a>
                        </p>

                    </div>
                </aside>

            </div>
            <div class="column3 footer-widget">
                <aside class="widget  widget_product_categories">
                    <h3 class="widget-title links-title">Product categories</h3>
                    <ul class="product-categories">

                        <li class="cat-item cat-item-5"><a href="http://localhost/test-project/category/aciform/">aciform</a>
                        </li>
                        <li class="cat-item cat-item-6"><a
                                    href="http://localhost/test-project/category/antiquarianism/">antiquarianism</a>
                        </li>
                        <li class="cat-item cat-item-7"><a href="http://localhost/test-project/category/arrangement/">arrangement</a>
                        </li>
                        <li class="cat-item cat-item-8"><a href="http://localhost/test-project/category/asmodeus/">asmodeus</a>
                        </li>
                        <li class="cat-item cat-item-10"><a
                                    href="http://localhost/test-project/category/broder/">broder</a
                        </li>
                        <li class="cat-item cat-item-11"><a
                                    href="http://localhost/test-project/category/buying/">buying</a
                        </li>
                    </ul>
                </aside>
            </div>
            <div class="column3 footer-widget">
                <aside class="widget  widget_product_categories">
                    <h3 class="widget-title links-title">Product categories</h3>
                    <ul class="product-categories">

                        <li class="cat-item cat-item-5"><a href="http://localhost/test-project/category/aciform/">aciform</a>
                        </li>
                        <li class="cat-item cat-item-6"><a
                                    href="http://localhost/test-project/category/antiquarianism/">antiquarianism</a>
                        </li>
                        <li class="cat-item cat-item-7"><a href="http://localhost/test-project/category/arrangement/">arrangement</a>
                        </li>
                        <li class="cat-item cat-item-8"><a href="http://localhost/test-project/category/asmodeus/">asmodeus</a>
                        </li>
                        <li class="cat-item cat-item-10"><a
                                    href="http://localhost/test-project/category/broder/">broder</a
                        </li>
                        <li class="cat-item cat-item-11"><a
                                    href="http://localhost/test-project/category/buying/">buying</a
                        </li>
                    </ul>
                </aside>
            </div>
            <div class="column3 footer-widget">
                <aside class="widget  widget_product_categories">
                    <h3 class="widget-title links-title">Product categories</h3>
                    <ul class="product-categories">

                        <li class="cat-item cat-item-5"><a href="http://localhost/test-project/category/aciform/">aciform</a>
                        </li>
                        <li class="cat-item cat-item-6"><a
                                    href="http://localhost/test-project/category/antiquarianism/">antiquarianism</a>
                        </li>
                        <li class="cat-item cat-item-7"><a href="http://localhost/test-project/category/arrangement/">arrangement</a>
                        </li>
                        <li class="cat-item cat-item-8"><a href="http://localhost/test-project/category/asmodeus/">asmodeus</a>
                        </li>
                        <li class="cat-item cat-item-10"><a
                                    href="http://localhost/test-project/category/broder/">broder</a
                        </li>
                        <li class="cat-item cat-item-11"><a
                                    href="http://localhost/test-project/category/buying/">buying</a
                        </li>
                    </ul>
                </aside>
            </div>
            </div>
        </div>
    </div>
    <div class="hitStore_main_footer_copyright">
        <div class="container">
            <div class="row">
                <div class="column6">
                    <p class="left">All Rights reserved. 2019 Pharmamandu Pvt Ltd</p>
                </div>
                <div class="column6">
                    <div class="payment_method right">
                        <img src="assets/images/payment-support.png" alt=""/>
                    </div>
                </div>
            </div>

        </div>
    </div>
</footer>

<div class="offset__wrapper">
    <div class="body__overlay"></div>

    <div class="user__meta">
        <div class="user__meta__inner">
            <div class="offsetmenu__user__meta__close__btn">
                <a href="#"><i class="fa fa-close"></i></a>
            </div>
            <div class="user__meta__button">
                <ul class="user__meta__btn">
                    <li><a href="#">Log In</a></li>
                    <li class="user__meta__signup"><a href="#">Sign Up</a></li>
                </ul>
            </div>
            <div class="account__menu">
                <div href="#" class="user__meta__setting"> User Setting</div>
                <ul class="user__meta__account__menu">
                    <li><a href="#">My Account</a></li>
                    <li><a href="#">My Profile</a></li>
                    <li><a href="#">Cart</a></li>
                    <li><a href="#">Checkout</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="shopping__cart">
        <div class="shopping__cart__inner">
            <div class="offsetmenu__close__btn">
                <a href="#"><i class="fa fa-close"></i></a>
            </div>
            <div class="shp__cart__wrap">
                <div class="shp__single__product">
                    <div class="shp__pro__thumb">
                        <a href="#">
                            <img src="assets/images/cart-img-1.png" alt="product images">
                        </a>
                    </div>
                    <div class="shp__pro__details">
                        <h2><a href="product-details.html">BO&amp;Play Wireless Speaker</a></h2>
                        <span class="quantity">QTY: 1</span>
                        <span class="shp__price">$105.00</span>
                    </div>
                    <div class="remove__btn">
                        <a href="#" title="Remove this item"><i class="fa fa-close"></i></a>
                    </div>
                </div>
                <div class="shp__single__product">
                    <div class="shp__pro__thumb">
                        <a href="#">
                            <img src="assets/images/cart-img-2.png" alt="product images">
                        </a>
                    </div>
                    <div class="shp__pro__details">
                        <h2><a href="product-details.html">Brone Candle</a></h2>
                        <span class="quantity">QTY: 1</span>
                        <span class="shp__price">$25.00</span>
                    </div>
                    <div class="remove__btn">
                        <a href="#" title="Remove this item"><i class="fa fa-close"></i></a>
                    </div>
                </div>
            </div>
            <ul class="shoping__total">
                <li class="subtotal">Subtotal:</li>
                <li class="total__price">$130.00</li>
            </ul>
            <ul class="shopping__btn">
                <li><a href="cart.html">View Cart</a></li>
                <li class="shp__checkout"><a href="checkout.html">Checkout</a></li>
            </ul>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="assets/library/lightslider/js/lightslider.js"></script>
<script src="assets/library/owlcarousel/js/owl.carousel.min.js"></script>
<script src="assets/library/tilt/tilt.jquery.js"></script>

<script src="assets/library/elevatezoom/jquery.elevatezoom.js"></script>
<script src="assets/js/main.js"></script>

</body>
</html>
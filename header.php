<!doctype html>
<html>
<head>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,700i,800"
          rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="assets/library/font-awesome/css/font-awesome.min.css"/>
    <link type="text/css" rel="stylesheet" href="assets/library/lightslider/css/lightslider.min.css"/>

    <link type="text/css" rel="stylesheet" href="assets/library/owlcarousel/css/owl.carousel.min.css"/>
    <link type="text/css" rel="stylesheet" href="assets/library/owlcarousel/css/owl.theme.default.min.css"/>
    <link rel="stylesheet" type="text/css" href="style.css"/>
    <link rel="stylesheet" type="text/css" href="assets/css/responsive.css"/>

</head>

<body>
<header>
    <div class="hitStore_topheader">
        <div class="container">
            <div class="row">
                <div class="column6 hitStore_topheader_left">
                    <div class="top_menu_wrapper">
                        <ul class="hitStore_top_menu">
                            <li><a href="">Home </a></li>
                            <li><a href="">About Us </a></li>
                            <li><a href="">Terms & Condition</a></li>
                            <li><a href="">Contact Us</a></li>
                        </ul>
                    </div>


                </div>
                <div class="column6 hitStore_topheader_right">
                    <ul class="hitstore_socialmeida_link">
                        <li>
                            <a href="#">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-google"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-instagram"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-pinterest"></i>
                            </a>
                        </li>

                        <li>
                            <a href="#">
                                <i class="fa fa-youtube"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="hitStore_mainheader">
        <div class="container">
            <div class="row">
                <div class="column4">
                    <div class="logo site-branding">
                        <a href="#" class="custom-logo-link"><img width="290" height="50"
                                                                  src="assets/images/web_logo.png" class="custom-logo"
                                                                  alt="Hitstoreqq"></a>
                        <!--<h1 class="site-title">
                            <a href="#" rel="home">Hit-Store</a>
                        </h1>
                        <p class="site-description">Just another WordPress site</p>-->
                    </div>
                </div>
                <div class="column6">
                    <div class="header_search_form">
                        <form>
                            <div class="search-box-border">
                                <div class="header_select_cata">
                                    <select>
                                        <option><a href="#">Electronics</a></option>
                                        <option><a href="#">Home Application</a></option>
                                        <option><a href="#">Sport & Outdoor</a></option>
                                    </select>
                                </div>
                                <div class="header_search_input_box">
                                    <input type="text" placeholder="Search.." name="s" id="s">
                                </div>
                                <button type="submit" class="header_product_search"><i class="fa fa-search"></i>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="column2">
                    <div class="htStore_ecommerce_extra_button">
                        <div class="hitStore_user_account">
                            <div class="hitStore_user_account_icon">
                                <span><i class="fa fa-user"></i></span>
                                <!--<div class="hitStore_account_signIn">
                                    Login / Sign up
                                </div>-->
                            </div>
                            <!--<div class="user_account_menu_dropdown">
                                <div class="hitStore_signIn hitstore_login">
                                    <a href="#" class=" hitStore_login_button">Login</a>
                                    <a href="#" class="hitStore_signIn_button">Join</a>
                                </div>
                                <div class="hitStore_account_menu_list">
                                    <ul>
                                        <li><a href="">My Account</a></li>
                                        <li><a href="">Cart</a></li>
                                        <li><a href="">Checkout</a></li>
                                        <li><a href="">Sign Out</a></li>
                                    </ul>
                                </div>
                            </div>-->
                        </div>
                        <div class="hitStore_user_wishlist">
                            <div class="hitStore_wishlist_icon">
                                <span><i class="fa fa-heart-o"></i></i>
                                    <div class="wishlist_count">5</div></span>

                            </div>
                        </div>
                        <div class="hitStore_user_cart">
                            <div class="hitStore_cart_icon">
                                <span><i class="fa fa-shopping-cart"></i>
                                <div class="cart_count">10</div></span>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <nav id="site-navigation" class="main-navigation">
        <div class="container">
            <button class="main-menu-toggle" aria-controls="primary-menu" aria-expanded="false"><i
                        class="fa fa-bars"></i></button>
            <div class="main-menu-container-collapse">
                <ul id="primary-menu" class="menu nav-menu" aria-expanded="false">
                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-210"><a
                                href="#">Home</a></li>
                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-246"><a
                                href="#">Shop</a></li>
                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-95">
                        <a href="#">Checkout</a></li>
                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-94">
                        <a href="#">My account</a>
                        <ul class="sub-menu">
                            <li class="btn menu-item menu-item-type-post_type menu-item-object-page menu-item-98"><a
                                        href="#">Sample Page</a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-96">
                                <a href="#">Cart</a>
                                <ul class="sub-menu">
                                    <li class="btn menu-item menu-item-type-post_type menu-item-object-page menu-item-97">
                                        <a href="#">Shop</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-95">
                        <a href="#">Contact Us</a></li>
                </ul>
            </div>
        </div>
    </nav>
</header>


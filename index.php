<?php include 'header.php' ?>

<section class="hitStore_category_with_banner">
    <div class="container">
        <div class="hitStore_main_category_nav hitStore_category_nav_left">
            <div class="hitStore_main_category_title">
                <h3> Product Category 22</h3>
            </div>
            <div class="menu-product-category-container">
                <ul id="category-menu" class="menu">
                    <li id="menu-item-276"
                        class="menu-item">
                        <a href="http://localhost/wordpress/category/home-appliances/">Home Appliances</a></li>
                    <li id="menu-item-278"
                        class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-278">
                        <a href="http://localhost/wordpress/category/mens-clothing/">Men`s Clothing</a></li>

                    <li id="menu-item-281"
                        class="menu-item  current-menu-item menu-item-has-children >
                        <a href="#">Watches &amp; Accessories</a>
                        <ul class="sub-menu">
                            <li id="menu-item-277"
                                class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-277"><a
                                        href="#">Kids Wear</a></li>
                            <li id="menu-item-282"
                                class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-282"><a
                                        href="#">Women`s Clothing</a></li>
                        </ul>
                    </li>

                    <li id="menu-item-279"
                        class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-279">
                        <a href="http://localhost/wordpress/category/sports-outdoor/">Sports &amp; Outdoor</a></li>
                    <li id="menu-item-283"
                        class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-283">
                        <a href="http://localhost/wordpress/category/automotive-motorbikes/">Automotive &amp;
                            Motorbikes</a>
                    </li>
                    <li id="menu-item-284"
                        class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-284">
                        <a href="http://localhost/wordpress/category/groceries/">Groceries</a></li>
                    <li id="menu-item-285"
                        class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-285">
                        <a href="http://localhost/wordpress/category/pets/">Pets</a></li>
                    <li id="menu-item-279"
                        class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-279">
                        <a href="http://localhost/wordpress/category/sports-outdoor/">Sports &amp; Outdoor</a></li>
                    <li id="menu-item-283"
                        class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-283">
                        <a href="http://localhost/wordpress/category/automotive-motorbikes/">Automotive &amp;
                            Motorbikes</a>
                    </li>
                    <li id="menu-item-284"
                        class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-284">
                        <a href="http://localhost/wordpress/category/groceries/">Groceries</a></li>

                </ul>
            </div>
        </div>
        <div class="hitStore_main_slider">
            <div class="hitStoreslider hitStore_slider_list">
                <div class="hitStore_slider_wrap">
                    <div class="hitStoreslider_image">
                        <img src="assets/images/banner.jpg">
                    </div>
                    <div class="hitStore_slider_content banner_content_left left">
                        <h3>New Product Collection</h3>
                        <div class="hitStore_slider_button">
                            <a href="#">Shop Now</a>
                        </div>
                    </div>
                </div>

                <div class="hitStore_slider_wrap">
                    <div class="hitStoreslider_image">
                        <img src="assets/images/banner2.jpg">
                    </div>
                    <div class="hitStore_slider_content banner_content_right right">
                        <h3>BEST ITEM <span>THIS SUMMER</span></h3>
                        <div class="hitStore_slider_button">
                            <a href="#">Shop Now</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <div class="hitStore_promotion_banner">
        <div class="container">
            <div class="row">
                <div class="box">
                    <figure>
                        <a href="#"><img src="assets/images/promo1.jpg" alt=""> </a>
                    </figure>
                </div>
                <div class="box">
                    <figure>
                        <a href="#"><img src="assets/images/promo2.jpg" alt=""> </a>
                    </figure>
                </div>
                <div class="box">
                    <figure>
                        <a href="#"><img src="assets/images/promo1.jpg" alt=""> </a>
                    </figure>
                </div>

            </div>
        </div>
    </div>
</section>


<section class="hitStore_main_feature">
    <div class="container">
        <div class="row">
            <div class="column12">
                <div class="section-title" data-title="Featured">
                    <h2>Featured</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column12">
                <div class="product-tab-filter">
                    <ul class="tabs">
                        <li class="tab-link current" data-tab="tab-1">All</li>
                        <li class="tab-link" data-tab="tab-2">Electronics</li>
                        <li class="tab-link" data-tab="tab-3">Sports</li>
                        <li class="tab-link" data-tab="tab-4">Mobile</li>
                    </ul>
                </div>
            </div>
            <div class="column12">
                <div class="product-tab-content">
                    <div id="tab-1" class="tab-content  current">
                        <div class="owl-carousel owl-theme bestseller-tab-1">
                            <div class="item">
                                <div class="product-list-inner">
                                    <div class="product-listing">
                                        <!-- Image -->
                                        <figure>
                                            <span class="label sale">sale</span>
                                            <a href="#" class="img"><img src="assets/images/product-1.png"
                                                                         alt="Product Image"></a>
                                            <div class="product_hover_info">
                                                <ul class="product_action">
                                                    <li><a title="View" href="#"><i class="fa fa-eye"></i></a>
                                                    </li>
                                                    <li><a title="Add TO Cart" href="#"><i
                                                                    class="fa fa-shopping-cart"></i></a>
                                                    </li>
                                                    <li><a title="Wishlist" href="#"><i class="fa fa-heart"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </figure>

                                        <!-- Content -->
                                        <div class="product-content">
                                            <!-- Category & Title -->
                                            <div class="product-category-title">
                                                <a href="#" class="product-cat">Camera</a>
                                                <h5 class="product-title"><a href="#">Mony Handycam Z 105</a></h5>
                                            </div>
                                            <!-- Price & Ratting -->
                                            <div class="price-ratting">
                                                <h5 class="price">$110.00</h5>
                                                <div class="ratting">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-half-o"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="product-list-inner">
                                    <div class="product-listing">
                                        <!-- Image -->
                                        <figure>
                                            <span class="label sale">sale</span>
                                            <a href="#" class="img"><img src="assets/images/product-2.png"
                                                                         alt="Product Image"></a>
                                            <div class="product_hover_info">
                                                <ul class="product_action">
                                                    <li><a title="View" href="#"><i class="fa fa-eye"></i></a>
                                                    </li>
                                                    <li><a title="Add TO Cart" href="#"><i
                                                                    class="fa fa-shopping-cart"></i></a>
                                                    </li>
                                                    <li><a title="Wishlist" href="#"><i class="fa fa-heart"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </figure>

                                        <!-- Content -->
                                        <div class="product-content">
                                            <!-- Category & Title -->
                                            <div class="product-category-title">
                                                <a href="#" class="product-cat">Camera</a>
                                                <h5 class="product-title"><a href="#">Mony Handycam Z 105</a></h5>
                                            </div>
                                            <!-- Price & Ratting -->
                                            <div class="price-ratting">
                                                <h5 class="price">$110.00</h5>
                                                <div class="ratting">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-half-o"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="product-list-inner">
                                    <div class="product-listing">
                                        <!-- Image -->
                                        <figure>
                                            <span class="label sale">sale</span>
                                            <a href="#" class="img"><img src="assets/images/product-3.png"
                                                                         alt="Product Image"></a>
                                            <div class="product_hover_info">
                                                <ul class="product_action">
                                                    <li><a title="View" href="#"><i class="fa fa-eye"></i></a>
                                                    </li>
                                                    <li><a title="Add TO Cart" href="#"><i
                                                                    class="fa fa-shopping-cart"></i></a>
                                                    </li>
                                                    <li><a title="Wishlist" href="#"><i class="fa fa-heart"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </figure>

                                        <!-- Content -->
                                        <div class="product-content">
                                            <!-- Category & Title -->
                                            <div class="product-category-title">
                                                <a href="#" class="product-cat">Camera</a>
                                                <h5 class="product-title"><a href="#">Mony Handycam Z 105</a></h5>
                                            </div>
                                            <!-- Price & Ratting -->
                                            <div class="price-ratting">
                                                <h5 class="price">$110.00</h5>
                                                <div class="ratting">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-half-o"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="product-list-inner">
                                    <div class="product-listing">
                                        <!-- Image -->
                                        <figure>
                                            <span class="label sale">sale</span>
                                            <a href="#" class="img"><img src="assets/images/product-4.png"
                                                                         alt="Product Image"></a>
                                            <div class="product_hover_info">
                                                <ul class="product_action">
                                                    <li><a title="View" href="#"><i class="fa fa-eye"></i></a>
                                                    </li>
                                                    <li><a title="Add TO Cart" href="#"><i
                                                                    class="fa fa-shopping-cart"></i></a>
                                                    </li>
                                                    <li><a title="Wishlist" href="#"><i class="fa fa-heart"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </figure>

                                        <!-- Content -->
                                        <div class="product-content">
                                            <!-- Category & Title -->
                                            <div class="product-category-title">
                                                <a href="#" class="product-cat">Camera</a>
                                                <h5 class="product-title"><a href="#">Mony Handycam Z 105</a></h5>
                                            </div>
                                            <!-- Price & Ratting -->
                                            <div class="price-ratting">
                                                <h5 class="price">$110.00</h5>
                                                <div class="ratting">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-half-o"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="tab-2" class="tab-content">
                        <div class="owl-carousel owl-theme bestseller-tab-2">
                            <div class="item">
                                <div class="product-list-inner">
                                    <div class="product-listing">
                                        <!-- Image -->
                                        <figure>
                                            <span class="label sale">sale</span>
                                            <a href="#" class="img"><img src="assets/images/product-5.png"
                                                                         alt="Product Image"></a>
                                            <div class="product_hover_info">
                                                <ul class="product_action">
                                                    <li><a title="View" href="#"><i class="fa fa-eye"></i></a>
                                                    </li>
                                                    <li><a title="Add TO Cart" href="#"><i
                                                                    class="fa fa-shopping-cart"></i></a>
                                                    </li>
                                                    <li><a title="Wishlist" href="#"><i class="fa fa-heart"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </figure>

                                        <!-- Content -->
                                        <div class="product-content">
                                            <!-- Category & Title -->
                                            <div class="product-category-title">
                                                <a href="#" class="product-cat">Camera</a>
                                                <h5 class="product-title"><a href="#">Mony Handycam Z 105</a></h5>
                                            </div>
                                            <!-- Price & Ratting -->
                                            <div class="price-ratting">
                                                <h5 class="price">$110.00</h5>
                                                <div class="ratting">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-half-o"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="product-list-inner">
                                    <div class="product-listing">
                                        <!-- Image -->
                                        <figure>
                                            <span class="label sale">sale</span>
                                            <a href="#" class="img"><img src="assets/images/product-6.png"
                                                                         alt="Product Image"></a>
                                            <div class="product_hover_info">
                                                <ul class="product_action">
                                                    <li><a title="View" href="#"><i class="fa fa-eye"></i></a>
                                                    </li>
                                                    <li><a title="Add TO Cart" href="#"><i
                                                                    class="fa fa-shopping-cart"></i></a>
                                                    </li>
                                                    <li><a title="Wishlist" href="#"><i class="fa fa-heart"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </figure>

                                        <!-- Content -->
                                        <div class="product-content">
                                            <!-- Category & Title -->
                                            <div class="product-category-title">
                                                <a href="#" class="product-cat">Camera</a>
                                                <h5 class="product-title"><a href="#">Mony Handycam Z 105</a></h5>
                                            </div>
                                            <!-- Price & Ratting -->
                                            <div class="price-ratting">
                                                <h5 class="price">$110.00</h5>
                                                <div class="ratting">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-half-o"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="product-list-inner">
                                    <div class="product-listing">
                                        <!-- Image -->
                                        <figure>
                                            <span class="label sale">sale</span>
                                            <a href="#" class="img"><img src="assets/images/product-3.png"
                                                                         alt="Product Image"></a>
                                            <div class="product_hover_info">
                                                <ul class="product_action">
                                                    <li><a title="View" href="#"><i class="fa fa-eye"></i></a>
                                                    </li>
                                                    <li><a title="Add TO Cart" href="#"><i
                                                                    class="fa fa-shopping-cart"></i></a>
                                                    </li>
                                                    <li><a title="Wishlist" href="#"><i class="fa fa-heart"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </figure>

                                        <!-- Content -->
                                        <div class="product-content">
                                            <!-- Category & Title -->
                                            <div class="product-category-title">
                                                <a href="#" class="product-cat">Camera</a>
                                                <h5 class="product-title"><a href="#">Mony Handycam Z 105</a></h5>
                                            </div>
                                            <!-- Price & Ratting -->
                                            <div class="price-ratting">
                                                <h5 class="price">$110.00</h5>
                                                <div class="ratting">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-half-o"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="product-list-inner">
                                    <div class="product-listing">
                                        <!-- Image -->
                                        <figure>
                                            <span class="label sale">sale</span>
                                            <a href="#" class="img"><img src="assets/images/product-4.png"
                                                                         alt="Product Image"></a>
                                            <div class="product_hover_info">
                                                <ul class="product_action">
                                                    <li><a title="View" href="#"><i class="fa fa-eye"></i></a>
                                                    </li>
                                                    <li><a title="Add TO Cart" href="#"><i
                                                                    class="fa fa-shopping-cart"></i></a>
                                                    </li>
                                                    <li><a title="Wishlist" href="#"><i class="fa fa-heart"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </figure>

                                        <!-- Content -->
                                        <div class="product-content">
                                            <!-- Category & Title -->
                                            <div class="product-category-title">
                                                <a href="#" class="product-cat">Camera</a>
                                                <h5 class="product-title"><a href="#">Mony Handycam Z 105</a></h5>
                                            </div>
                                            <!-- Price & Ratting -->
                                            <div class="price-ratting">
                                                <h5 class="price">$110.00</h5>
                                                <div class="ratting">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-half-o"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="tab-3" class="tab-content">
                        <div class="owl-carousel owl-theme bestseller-tab-3">
                            <div class="item">
                                <div class="product-list-inner">
                                    <div class="product-listing">
                                        <!-- Image -->
                                        <figure>
                                            <span class="label sale">sale</span>
                                            <a href="#" class="img"><img src="assets/images/product-1.png"
                                                                         alt="Product Image"></a>
                                            <div class="product_hover_info">
                                                <ul class="product_action">
                                                    <li><a title="View" href="#"><i class="fa fa-eye"></i></a>
                                                    </li>
                                                    <li><a title="Add TO Cart" href="#"><i
                                                                    class="fa fa-shopping-cart"></i></a>
                                                    </li>
                                                    <li><a title="Wishlist" href="#"><i class="fa fa-heart"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </figure>

                                        <!-- Content -->
                                        <div class="product-content">
                                            <!-- Category & Title -->
                                            <div class="product-category-title">
                                                <a href="#" class="product-cat">Camera</a>
                                                <h5 class="product-title"><a href="#">Mony Handycam Z 105</a></h5>
                                            </div>
                                            <!-- Price & Ratting -->
                                            <div class="price-ratting">
                                                <h5 class="price">$110.00</h5>
                                                <div class="ratting">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-half-o"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="product-list-inner">
                                    <div class="product-listing">
                                        <!-- Image -->
                                        <figure>
                                            <span class="label sale">sale</span>
                                            <a href="#" class="img"><img src="assets/images/product-2.png"
                                                                         alt="Product Image"></a>
                                            <div class="product_hover_info">
                                                <ul class="product_action">
                                                    <li><a title="View" href="#"><i class="fa fa-eye"></i></a>
                                                    </li>
                                                    <li><a title="Add TO Cart" href="#"><i
                                                                    class="fa fa-shopping-cart"></i></a>
                                                    </li>
                                                    <li><a title="Wishlist" href="#"><i class="fa fa-heart"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </figure>

                                        <!-- Content -->
                                        <div class="product-content">
                                            <!-- Category & Title -->
                                            <div class="product-category-title">
                                                <a href="#" class="product-cat">Camera</a>
                                                <h5 class="product-title"><a href="#">Mony Handycam Z 105</a></h5>
                                            </div>
                                            <!-- Price & Ratting -->
                                            <div class="price-ratting">
                                                <h5 class="price">$110.00</h5>
                                                <div class="ratting">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-half-o"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="product-list-inner">
                                    <div class="product-listing">
                                        <!-- Image -->
                                        <figure>
                                            <span class="label sale">sale</span>
                                            <a href="#" class="img"><img src="assets/images/product-3.png"
                                                                         alt="Product Image"></a>
                                            <div class="product_hover_info">
                                                <ul class="product_action">
                                                    <li><a title="View" href="#"><i class="fa fa-eye"></i></a>
                                                    </li>
                                                    <li><a title="Add TO Cart" href="#"><i
                                                                    class="fa fa-shopping-cart"></i></a>
                                                    </li>
                                                    <li><a title="Wishlist" href="#"><i class="fa fa-heart"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </figure>

                                        <!-- Content -->
                                        <div class="product-content">
                                            <!-- Category & Title -->
                                            <div class="product-category-title">
                                                <a href="#" class="product-cat">Camera</a>
                                                <h5 class="product-title"><a href="#">Mony Handycam Z 105</a></h5>
                                            </div>
                                            <!-- Price & Ratting -->
                                            <div class="price-ratting">
                                                <h5 class="price">$110.00</h5>
                                                <div class="ratting">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-half-o"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="product-list-inner">
                                    <div class="product-listing">
                                        <!-- Image -->
                                        <figure>
                                            <span class="label sale">sale</span>
                                            <a href="#" class="img"><img src="assets/images/product-4.png"
                                                                         alt="Product Image"></a>
                                            <div class="product_hover_info">
                                                <ul class="product_action">
                                                    <li><a title="View" href="#"><i class="fa fa-eye"></i></a>
                                                    </li>
                                                    <li><a title="Add TO Cart" href="#"><i
                                                                    class="fa fa-shopping-cart"></i></a>
                                                    </li>
                                                    <li><a title="Wishlist" href="#"><i class="fa fa-heart"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </figure>

                                        <!-- Content -->
                                        <div class="product-content">
                                            <!-- Category & Title -->
                                            <div class="product-category-title">
                                                <a href="#" class="product-cat">Camera</a>
                                                <h5 class="product-title"><a href="#">Mony Handycam Z 105</a></h5>
                                            </div>
                                            <!-- Price & Ratting -->
                                            <div class="price-ratting">
                                                <h5 class="price">$110.00</h5>
                                                <div class="ratting">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-half-o"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="tab-4" class="tab-content">
                        <div class="owl-carousel owl-theme bestseller-tab-4">
                            <div class="item">
                                <div class="product-list-inner">
                                    <div class="product-listing">
                                        <!-- Image -->
                                        <figure>
                                            <span class="label sale">sale</span>
                                            <a href="#" class="img"><img src="assets/images/product-5.png"
                                                                         alt="Product Image"></a>
                                            <div class="product_hover_info">
                                                <ul class="product_action">
                                                    <li><a title="View" href="#"><i class="fa fa-eye"></i></a>
                                                    </li>
                                                    <li><a title="Add TO Cart" href="#"><i
                                                                    class="fa fa-shopping-cart"></i></a>
                                                    </li>
                                                    <li><a title="Wishlist" href="#"><i class="fa fa-heart"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </figure>

                                        <!-- Content -->
                                        <div class="product-content">
                                            <!-- Category & Title -->
                                            <div class="product-category-title">
                                                <a href="#" class="product-cat">Camera</a>
                                                <h5 class="product-title"><a href="#">Mony Handycam Z 105</a></h5>
                                            </div>
                                            <!-- Price & Ratting -->
                                            <div class="price-ratting">
                                                <h5 class="price">$110.00</h5>
                                                <div class="ratting">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-half-o"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="product-list-inner">
                                    <div class="product-listing">
                                        <!-- Image -->
                                        <figure>
                                            <span class="label sale">sale</span>
                                            <a href="#" class="img"><img src="assets/images/product-6.png"
                                                                         alt="Product Image"></a>
                                            <div class="product_hover_info">
                                                <ul class="product_action">
                                                    <li><a title="View" href="#"><i class="fa fa-eye"></i></a>
                                                    </li>
                                                    <li><a title="Add TO Cart" href="#"><i
                                                                    class="fa fa-shopping-cart"></i></a>
                                                    </li>
                                                    <li><a title="Wishlist" href="#"><i class="fa fa-heart"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </figure>

                                        <!-- Content -->
                                        <div class="product-content">
                                            <!-- Category & Title -->
                                            <div class="product-category-title">
                                                <a href="#" class="product-cat">Camera</a>
                                                <h5 class="product-title"><a href="#">Mony Handycam Z 105</a></h5>
                                            </div>
                                            <!-- Price & Ratting -->
                                            <div class="price-ratting">
                                                <h5 class="price">$110.00</h5>
                                                <div class="ratting">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-half-o"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="product-list-inner">
                                    <div class="product-listing">
                                        <!-- Image -->
                                        <figure>
                                            <span class="label sale">sale</span>
                                            <a href="#" class="img"><img src="assets/images/product-3.png"
                                                                         alt="Product Image"></a>
                                            <div class="product_hover_info">
                                                <ul class="product_action">
                                                    <li><a title="View" href="#"><i class="fa fa-eye"></i></a>
                                                    </li>
                                                    <li><a title="Add TO Cart" href="#"><i
                                                                    class="fa fa-shopping-cart"></i></a>
                                                    </li>
                                                    <li><a title="Wishlist" href="#"><i class="fa fa-heart"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </figure>

                                        <!-- Content -->
                                        <div class="product-content">
                                            <!-- Category & Title -->
                                            <div class="product-category-title">
                                                <a href="#" class="product-cat">Camera</a>
                                                <h5 class="product-title"><a href="#">Mony Handycam Z 105</a></h5>
                                            </div>
                                            <!-- Price & Ratting -->
                                            <div class="price-ratting">
                                                <h5 class="price">$110.00</h5>
                                                <div class="ratting">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-half-o"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="product-list-inner">
                                    <div class="product-listing">
                                        <!-- Image -->
                                        <figure>
                                            <span class="label sale">sale</span>
                                            <a href="#" class="img"><img src="assets/images/product-4.png"
                                                                         alt="Product Image"></a>
                                            <div class="product_hover_info">
                                                <ul class="product_action">
                                                    <li><a title="View" href="#"><i class="fa fa-eye"></i></a>
                                                    </li>
                                                    <li><a title="Add TO Cart" href="#"><i
                                                                    class="fa fa-shopping-cart"></i></a>
                                                    </li>
                                                    <li><a title="Wishlist" href="#"><i class="fa fa-heart"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </figure>

                                        <!-- Content -->
                                        <div class="product-content">
                                            <!-- Category & Title -->
                                            <div class="product-category-title">
                                                <a href="#" class="product-cat">Camera</a>
                                                <h5 class="product-title"><a href="#">Mony Handycam Z 105</a></h5>
                                            </div>
                                            <!-- Price & Ratting -->
                                            <div class="price-ratting">
                                                <h5 class="price">$110.00</h5>
                                                <div class="ratting">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-half-o"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="hitStore_main_new_arrivals">
    <div class="container">
        <div class="row">
            <div class="column12">
                <div class="section-title" data-title="New Arrivals">
                    <h2>New Arrivals</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column12">
                <div class="box">
                    <div class="product-list-inner">
                        <div class="product-listing">
                            <!-- Image -->
                            <figure>
                                <span class="label sale">sale</span>
                                <a href="#" class="img"><img src="assets/images/product-2.png" alt="Product Image"></a>
                                <div class="product_hover_info">
                                    <ul class="product_action">
                                        <li><a title="View" href="#"><i class="fa fa-eye"></i></a>
                                        </li>
                                        <li><a title="Add TO Cart" href="#"><i class="fa fa-shopping-cart"></i></a>
                                        </li>
                                        <li><a title="Wishlist" href="#"><i class="fa fa-heart"></i></a></li>
                                    </ul>
                                </div>
                            </figure>

                            <!-- Content -->
                            <div class="product-content">
                                <!-- Category & Title -->
                                <div class="product-category-title">
                                    <a href="#" class="product-cat">Camera</a>
                                    <h5 class="product-title"><a href="#">Mony Handycam Z 105</a></h5>
                                </div>
                                <!-- Price & Ratting -->
                                <div class="price-ratting">
                                    <h5 class="price"><span class="old-price">$200</span>$110.00 asdfasdfasdfsadfsd</h5>
                                    <div class="ratting">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half-o"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="product-list-inner">
                        <div class="product-listing">
                            <!-- Image -->
                            <figure>
                                <span class="label sale">sale</span>
                                <a href="#" class="img"><img src="assets/images/product-2.png" alt="Product Image"></a>
                                <div class="product_hover_info">
                                    <ul class="product_action">
                                        <li><a title="View" href="#"><i class="fa fa-eye"></i></a>
                                        </li>
                                        <li><a title="Add TO Cart" href="#"><i class="fa fa-shopping-cart"></i></a>
                                        </li>
                                        <li><a title="Wishlist" href="#"><i class="fa fa-heart"></i></a></li>
                                    </ul>
                                </div>
                            </figure>

                            <!-- Content -->
                            <div class="product-content">
                                <!-- Category & Title -->
                                <div class="product-category-title">
                                    <a href="#" class="product-cat">Camera</a>
                                    <h5 class="product-title"><a href="#">Mony Handycam Z 105</a></h5>
                                </div>
                                <!-- Price & Ratting -->
                                <div class="price-ratting">
                                    <h5 class="price">$110.00</h5>
                                    <div class="ratting">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half-o"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="product-list-inner">
                        <div class="product-listing">
                            <!-- Image -->
                            <figure>
                                <span class="label sale">sale</span>
                                <a href="#" class="img"><img src="assets/images/product-2.png" alt="Product Image"></a>
                                <div class="product_hover_info">
                                    <ul class="product_action">
                                        <li><a title="View" href="#"><i class="fa fa-eye"></i></a>
                                        </li>
                                        <li><a title="Add TO Cart" href="#"><i class="fa fa-shopping-cart"></i></a>
                                        </li>
                                        <li><a title="Wishlist" href="#"><i class="fa fa-heart"></i></a></li>
                                    </ul>
                                </div>
                            </figure>

                            <!-- Content -->
                            <div class="product-content">
                                <!-- Category & Title -->
                                <div class="product-category-title">
                                    <a href="#" class="product-cat">Camera</a>
                                    <h5 class="product-title"><a href="#">Mony Handycam Z 105</a></h5>
                                </div>
                                <!-- Price & Ratting -->
                                <div class="price-ratting">
                                    <h5 class="price">$110.00</h5>
                                    <div class="ratting">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half-o"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="product-list-inner">
                        <div class="product-listing">
                            <!-- Image -->
                            <figure>
                                <span class="label sale">sale</span>
                                <a href="#" class="img"><img src="assets/images/product-2.png" alt="Product Image"></a>
                                <div class="product_hover_info">
                                    <ul class="product_action">
                                        <li><a title="View" href="#"><i class="fa fa-eye"></i></a>
                                        </li>
                                        <li><a title="Add TO Cart" href="#"><i class="fa fa-shopping-cart"></i></a>
                                        </li>
                                        <li><a title="Wishlist" href="#"><i class="fa fa-heart"></i></a></li>
                                    </ul>
                                </div>
                            </figure>

                            <!-- Content -->
                            <div class="product-content">
                                <!-- Category & Title -->
                                <div class="product-category-title">
                                    <a href="#" class="product-cat">Camera</a>
                                    <h5 class="product-title"><a href="#">Mony Handycam Z 105</a></h5>
                                </div>
                                <!-- Price & Ratting -->
                                <div class="price-ratting">
                                    <h5 class="price">$110.00</h5>
                                    <div class="ratting">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half-o"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="product-list-inner">
                        <div class="product-listing">
                            <!-- Image -->
                            <figure>
                                <span class="label sale">sale</span>
                                <a href="#" class="img"><img src="assets/images/product-2.png" alt="Product Image"></a>
                                <div class="product_hover_info">
                                    <ul class="product_action">
                                        <li><a title="View" href="#"><i class="fa fa-eye"></i></a>
                                        </li>
                                        <li><a title="Add TO Cart" href="#"><i class="fa fa-shopping-cart"></i></a>
                                        </li>
                                        <li><a title="Wishlist" href="#"><i class="fa fa-heart"></i></a></li>
                                    </ul>
                                </div>
                            </figure>

                            <!-- Content -->
                            <div class="product-content">
                                <!-- Category & Title -->
                                <div class="product-category-title">
                                    <a href="#" class="product-cat">Camera</a>
                                    <h5 class="product-title"><a href="#">Mony Handycam Z 105</a></h5>
                                </div>
                                <!-- Price & Ratting -->
                                <div class="price-ratting">
                                    <h5 class="price">$110.00</h5>
                                    <div class="ratting">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half-o"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="hitStore_advertisement_banner">
    <div class="row">
        <div class="column6">
            <div class="advertisement">
                <figure>
                    <img src="assets/images/adz-1.jpg" alt="">
                    <div class="advertisement-caption">
                        <div class="advertisement-caption-inner js-tilt" data-tilt-perspective="300"
                             data-tilt-speed="400" data-tilt-max="25">
                            <h3>50% discount</h3>
                            <a href="#">Shop Now</a>
                        </div>

                    </div>
                </figure>
            </div>
        </div>
        <div class="column6">
            <div class="advertisement">
                <figure>
                    <img src="assets/images/adz-2.jpg" alt="">
                    <div class="advertisement-caption">
                        <div class="advertisement-caption-inner  js-tilt" data-tilt-perspective="300"
                             data-tilt-speed="400" data-tilt-max="25">
                            <h3>Best Price you can get </h3>
                            <a href="#">Shop Now</a>
                        </div>

                    </div>
                </figure>
            </div>
        </div>
    </div>
</section>


<section class="hitStore_main_product_with_side_banner">
    <div class="container">
        <div class="row">
            <div class="column12">
            <div class="section-title" data-title="New Arrivals">
                <h2>New Arrivals</h2>
            </div>
            </div>
        </div>
        <div class="row">
            <div class="column3">
                <div class="feature-adz-wrapper">
                    <div class="feature-adz" style="background:#2b5480">
                        <div class="feature-adz-inner js-tilt" data-tilt-perspective="1000"
                             data-tilt-speed="400" data-tilt-max="25">
                            <h4>Up to <span>30%</span> OFF</h4>
                            <div class="price">
                                <h6><span class="old-price">$200</span> $70</h6>
                            </div>
                            <img src="assets/images/product-4.png" alt="">
                            <h5 class="product-title"><a href="#">Mony Handycam Z 105</a></h5>

                            <a href="" class="shop-now">Shop Now</a>
                        </div>
                    </div>
                </div>
                <div class="feature-adz-wrapper">
                    <div class="feature-adz" style="background:#48a06e">
                        <div class="feature-adz-inner js-tilt" data-tilt-perspective="1000"
                             data-tilt-speed="400" data-tilt-max="25">
                            <h4>Up to <span>30%</span> OFF</h4>
                            <div class="price">
                                <h6><span class="old-price">$200</span> $70</h6>
                            </div>
                            <img src="assets/images/product-4.png" alt="">
                            <h5 class="product-title"><a href="#">Mony Handycam Z 105</a></h5>

                            <a href="" class="shop-now">Shop Now</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="column9">
                <div class="box">
                    <div class="product-list-inner">
                        <div class="product-listing">
                            <!-- Image -->
                            <figure>
                                <span class="label sale">sale</span>
                                <a href="#" class="img"><img src="assets/images/product-2.png" alt="Product Image"></a>
                                <div class="product_hover_info">
                                    <ul class="product_action">
                                        <li><a title="View" href="#"><i class="fa fa-eye"></i></a>
                                        </li>
                                        <li><a title="Add TO Cart" href="#"><i class="fa fa-shopping-cart"></i></a>
                                        </li>
                                        <li><a title="Wishlist" href="#"><i class="fa fa-heart"></i></a></li>
                                    </ul>
                                </div>
                            </figure>

                            <!-- Content -->
                            <div class="product-content">
                                <!-- Category & Title -->
                                <div class="product-category-title">
                                    <a href="#" class="product-cat">Camera</a>
                                    <h5 class="product-title"><a href="#">Mony Handycam Z 105</a></h5>
                                </div>
                                <!-- Price & Ratting -->
                                <div class="price-ratting">
                                    <h5 class="price">$110.00</h5>
                                    <div class="ratting">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half-o"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="product-list-inner">
                        <div class="product-listing">
                            <!-- Image -->
                            <figure>
                                <span class="label discount">20% Off</span>
                                <a href="#" class="img"><img src="assets/images/product-2.png" alt="Product Image"></a>
                                <div class="product_hover_info">
                                    <ul class="product_action">
                                        <li><a title="View" href="#"><i class="fa fa-eye"></i></a>
                                        </li>
                                        <li><a title="Add TO Cart" href="#"><i class="fa fa-shopping-cart"></i></a>
                                        </li>
                                        <li><a title="Wishlist" href="#"><i class="fa fa-heart"></i></a></li>
                                    </ul>
                                </div>
                            </figure>

                            <!-- Content -->
                            <div class="product-content">
                                <!-- Category & Title -->
                                <div class="product-category-title">
                                    <a href="#" class="product-cat">Camera</a>
                                    <h5 class="product-title"><a href="#">Mony Handycam Z 105</a></h5>
                                </div>
                                <!-- Price & Ratting -->
                                <div class="price-ratting">
                                    <h5 class="price">$110.00</h5>
                                    <div class="ratting">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half-o"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="product-list-inner">
                        <div class="product-listing">
                            <!-- Image -->
                            <figure>
                                <span class="label sale">sale</span>
                                <a href="#" class="img"><img src="assets/images/product-2.png" alt="Product Image"></a>
                                <div class="product_hover_info">
                                    <ul class="product_action">
                                        <li><a title="View" href="#"><i class="fa fa-eye"></i></a>
                                        </li>
                                        <li><a title="Add TO Cart" href="#"><i class="fa fa-shopping-cart"></i></a>
                                        </li>
                                        <li><a title="Wishlist" href="#"><i class="fa fa-heart"></i></a></li>
                                    </ul>
                                </div>
                            </figure>

                            <!-- Content -->
                            <div class="product-content">
                                <!-- Category & Title -->
                                <div class="product-category-title">
                                    <a href="#" class="product-cat">Camera</a>
                                    <h5 class="product-title"><a href="#">Mony Handycam Z 105</a></h5>
                                </div>
                                <!-- Price & Ratting -->
                                <div class="price-ratting">
                                    <h5 class="price">$110.00</h5>
                                    <div class="ratting">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half-o"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="product-list-inner">
                        <div class="product-listing">
                            <!-- Image -->
                            <figure>
                                <span class="label sale">sale</span>
                                <a href="#" class="img"><img src="assets/images/product-2.png" alt="Product Image"></a>
                                <div class="product_hover_info">
                                    <ul class="product_action">
                                        <li><a title="View" href="#"><i class="fa fa-eye"></i></a>
                                        </li>
                                        <li><a title="Add TO Cart" href="#"><i class="fa fa-shopping-cart"></i></a>
                                        </li>
                                        <li><a title="Wishlist" href="#"><i class="fa fa-heart"></i></a></li>
                                    </ul>
                                </div>
                            </figure>

                            <!-- Content -->
                            <div class="product-content">
                                <!-- Category & Title -->
                                <div class="product-category-title">
                                    <a href="#" class="product-cat">Camera</a>
                                    <h5 class="product-title"><a href="#">Mony Handycam Z 105</a></h5>
                                </div>
                                <!-- Price & Ratting -->
                                <div class="price-ratting">
                                    <h5 class="price">$110.00</h5>
                                    <div class="ratting">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half-o"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="product-list-inner">
                        <div class="product-listing">
                            <!-- Image -->
                            <figure>
                                <span class="label sale">sale</span>
                                <a href="#" class="img"><img src="assets/images/product-2.png" alt="Product Image"></a>
                                <div class="product_hover_info">
                                    <ul class="product_action">
                                        <li><a title="View" href="#"><i class="fa fa-eye"></i></a>
                                        </li>
                                        <li><a title="Add TO Cart" href="#"><i class="fa fa-shopping-cart"></i></a>
                                        </li>
                                        <li><a title="Wishlist" href="#"><i class="fa fa-heart"></i></a></li>
                                    </ul>
                                </div>
                            </figure>

                            <!-- Content -->
                            <div class="product-content">
                                <!-- Category & Title -->
                                <div class="product-category-title">
                                    <a href="#" class="product-cat">Camera</a>
                                    <h5 class="product-title"><a href="#">Mony Handycam Z 105</a></h5>
                                </div>
                                <!-- Price & Ratting -->
                                <div class="price-ratting">
                                    <h5 class="price">$110.00</h5>
                                    <div class="ratting">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half-o"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="product-list-inner">
                        <div class="product-listing">
                            <!-- Image -->
                            <figure>
                                <span class="label sale">sale</span>
                                <a href="#" class="img"><img src="assets/images/product-2.png" alt="Product Image"></a>
                                <div class="product_hover_info">
                                    <ul class="product_action">
                                        <li><a title="View" href="#"><i class="fa fa-eye"></i></a>
                                        </li>
                                        <li><a title="Add TO Cart" href="#"><i class="fa fa-shopping-cart"></i></a>
                                        </li>
                                        <li><a title="Wishlist" href="#"><i class="fa fa-heart"></i></a></li>
                                    </ul>
                                </div>
                            </figure>

                            <!-- Content -->
                            <div class="product-content">
                                <!-- Category & Title -->
                                <div class="product-category-title">
                                    <a href="#" class="product-cat">Camera</a>
                                    <h5 class="product-title"><a href="#">Mony Handycam Z 105</a></h5>
                                </div>
                                <!-- Price & Ratting -->
                                <div class="price-ratting">
                                    <h5 class="price">$110.00</h5>
                                    <div class="ratting">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half-o"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>


<section class="hitStore_advertisement_video" style="background: url('assets/images/adz-video.jpg')">
    <div class="container">
        <div class="row">
            <div class="column12">
            <div class="box">
                <div class="advertisement">
                    <div class="advertisement-caption-inner js-tilt" data-tilt-perspective="300"
                         data-tilt-speed="400" data-tilt-max="25">
                        <h3>Lord of the street</h3>
                        <h4>Fz-V25-fuel injector</h4>
                        <a href="#">Shop Now</a>
                    </div>
                </div>
            </div>
            <div class="box">
                <div class="video-container">
                    <iframe width="853" height="480"
                            src="https://www.youtube.com/embed/74Cm1p3fr0g?rel=0;&autoplay=1&mute=1"
                            allowfullscreen></iframe>
                </div>
            </div>
            </div>
        </div>
    </div>
</section>

<section class="hitStore_main_product_with_side_banner">
    <div class="container">
        <div class="row">
            <div class="column12">
            <div class="section-title" data-title="New Arrivals">
                <h2>New Arrivals</h2>
            </div>
            </div>
        </div>
        <div class="row">
            <div class="column3">
                <div class="feature-adz-wrapper">
                    <div class="feature-adz" style="background:#2b5480">
                        <div class="feature-adz-inner js-tilt" data-tilt-perspective="1000"
                             data-tilt-speed="400" data-tilt-max="25">
                            <h4>Up to <span>30%</span> OFF</h4>
                            <div class="price">
                                <h6><span class="old-price">$200</span> $70</h6>
                            </div>
                            <img src="assets/images/product-4.png" alt="">
                            <h5 class="product-title"><a href="#">Mony Handycam Z 105</a></h5>

                            <a href="" class="shop-now">Shop Now</a>
                        </div>
                    </div>
                </div>
                <div class="feature-adz-wrapper">
                    <div class="feature-adz" style="background:#48a06e">

                        <div class="feature-adz-inner js-tilt" data-tilt-perspective="1000"
                             data-tilt-speed="400" data-tilt-max="25">
                            <h4>Up to <span>30%</span> OFF</h4>
                            <div class="price">
                                <h6><span class="old-price">$200</span> $70</h6>
                            </div>
                            <img src="assets/images/product-4.png" alt="">
                            <h5 class="product-title"><a href="#">Mony Handycam Z 105</a></h5>

                            <a href="" class="shop-now">Shop Now</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="column9">
                <div class="box">
                    <div class="product-list-inner">
                        <div class="product-listing">
                            <!-- Image -->
                            <figure>
                                <span class="label sale">sale</span>
                                <a href="#" class="img"><img src="assets/images/product-2.png" alt="Product Image"></a>
                                <div class="product_hover_info">
                                    <ul class="product_action">
                                        <li><a title="View" href="#"><i class="fa fa-eye"></i></a>
                                        </li>
                                        <li><a title="Add TO Cart" href="#"><i class="fa fa-shopping-cart"></i></a>
                                        </li>
                                        <li><a title="Wishlist" href="#"><i class="fa fa-heart"></i></a></li>
                                    </ul>
                                </div>
                            </figure>

                            <!-- Content -->
                            <div class="product-content">
                                <!-- Category & Title -->
                                <div class="product-category-title">
                                    <a href="#" class="product-cat">Camera</a>
                                    <h5 class="product-title"><a href="#">Mony Handycam Z 105</a></h5>
                                </div>
                                <!-- Price & Ratting -->
                                <div class="price-ratting">
                                    <h5 class="price">$110.00</h5>
                                    <div class="ratting">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half-o"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="product-list-inner">
                        <div class="product-listing">
                            <!-- Image -->
                            <figure>
                                <span class="label sale">sale</span>
                                <a href="#" class="img"><img src="assets/images/product-2.png" alt="Product Image"></a>
                                <div class="product_hover_info">
                                    <ul class="product_action">
                                        <li><a title="View" href="#"><i class="fa fa-eye"></i></a>
                                        </li>
                                        <li><a title="Add TO Cart" href="#"><i class="fa fa-shopping-cart"></i></a>
                                        </li>
                                        <li><a title="Wishlist" href="#"><i class="fa fa-heart"></i></a></li>
                                    </ul>
                                </div>
                            </figure>

                            <!-- Content -->
                            <div class="product-content">
                                <!-- Category & Title -->
                                <div class="product-category-title">
                                    <a href="#" class="product-cat">Camera</a>
                                    <h5 class="product-title"><a href="#">Mony Handycam Z 105</a></h5>
                                </div>
                                <!-- Price & Ratting -->
                                <div class="price-ratting">
                                    <h5 class="price">$110.00</h5>
                                    <div class="ratting">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half-o"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="product-list-inner">
                        <div class="product-listing">
                            <!-- Image -->
                            <figure>
                                <span class="label sale">sale</span>
                                <a href="#" class="img"><img src="assets/images/product-2.png" alt="Product Image"></a>
                                <div class="product_hover_info">
                                    <ul class="product_action">
                                        <li><a title="View" href="#"><i class="fa fa-eye"></i></a>
                                        </li>
                                        <li><a title="Add TO Cart" href="#"><i class="fa fa-shopping-cart"></i></a>
                                        </li>
                                        <li><a title="Wishlist" href="#"><i class="fa fa-heart"></i></a></li>
                                    </ul>
                                </div>
                            </figure>

                            <!-- Content -->
                            <div class="product-content">
                                <!-- Category & Title -->
                                <div class="product-category-title">
                                    <a href="#" class="product-cat">Camera</a>
                                    <h5 class="product-title"><a href="#">Mony Handycam Z 105</a></h5>
                                </div>
                                <!-- Price & Ratting -->
                                <div class="price-ratting">
                                    <h5 class="price">$110.00</h5>
                                    <div class="ratting">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half-o"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="product-list-inner">
                        <div class="product-listing">
                            <!-- Image -->
                            <figure>
                                <span class="label sale">sale</span>
                                <a href="#" class="img"><img src="assets/images/product-2.png" alt="Product Image"></a>
                                <div class="product_hover_info">
                                    <ul class="product_action">
                                        <li><a title="View" href="#"><i class="fa fa-eye"></i></a>
                                        </li>
                                        <li><a title="Add TO Cart" href="#"><i class="fa fa-shopping-cart"></i></a>
                                        </li>
                                        <li><a title="Wishlist" href="#"><i class="fa fa-heart"></i></a></li>
                                    </ul>
                                </div>
                            </figure>

                            <!-- Content -->
                            <div class="product-content">
                                <!-- Category & Title -->
                                <div class="product-category-title">
                                    <a href="#" class="product-cat">Camera</a>
                                    <h5 class="product-title"><a href="#">Mony Handycam Z 105</a></h5>
                                </div>
                                <!-- Price & Ratting -->
                                <div class="price-ratting">
                                    <h5 class="price">$110.00</h5>
                                    <div class="ratting">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half-o"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="product-list-inner">
                        <div class="product-listing">
                            <!-- Image -->
                            <figure>
                                <span class="label sale">sale</span>
                                <a href="#" class="img"><img src="assets/images/product-2.png" alt="Product Image"></a>
                                <div class="product_hover_info">
                                    <ul class="product_action">
                                        <li><a title="View" href="#"><i class="fa fa-eye"></i></a>
                                        </li>
                                        <li><a title="Add TO Cart" href="#"><i class="fa fa-shopping-cart"></i></a>
                                        </li>
                                        <li><a title="Wishlist" href="#"><i class="fa fa-heart"></i></a></li>
                                    </ul>
                                </div>
                            </figure>

                            <!-- Content -->
                            <div class="product-content">
                                <!-- Category & Title -->
                                <div class="product-category-title">
                                    <a href="#" class="product-cat">Camera</a>
                                    <h5 class="product-title"><a href="#">Mony Handycam Z 105</a></h5>
                                </div>
                                <!-- Price & Ratting -->
                                <div class="price-ratting">
                                    <h5 class="price">$110.00</h5>
                                    <div class="ratting">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half-o"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="product-list-inner">
                        <div class="product-listing">
                            <!-- Image -->
                            <figure>
                                <span class="label sale">sale</span>
                                <a href="#" class="img"><img src="assets/images/product-2.png" alt="Product Image"></a>
                                <div class="product_hover_info">
                                    <ul class="product_action">
                                        <li><a title="View" href="#"><i class="fa fa-eye"></i></a>
                                        </li>
                                        <li><a title="Add TO Cart" href="#"><i class="fa fa-shopping-cart"></i></a>
                                        </li>
                                        <li><a title="Wishlist" href="#"><i class="fa fa-heart"></i></a></li>
                                    </ul>
                                </div>
                            </figure>

                            <!-- Content -->
                            <div class="product-content">
                                <!-- Category & Title -->
                                <div class="product-category-title">
                                    <a href="#" class="product-cat">Camera</a>
                                    <h5 class="product-title"><a href="#">Mony Handycam Z 105</a></h5>
                                </div>
                                <!-- Price & Ratting -->
                                <div class="price-ratting">
                                    <h5 class="price">$110.00</h5>
                                    <div class="ratting">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half-o"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<?php include 'footer.php' ?>
